﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Login_MVC_.Models;

namespace Login_MVC_.DLayer
{
    public class UserDal : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("UserAccounts");
        }

        public DbSet<User> UserAccounts { get; set; }

        public DbSet<webpages_Membership> webpages_Memberships { get; set; }

    }
}