﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Login_MVC_.Models;
using Login_MVC_.Utilities;
using RestSharp;

namespace Login_MVC_.ApiProxy
{
    public class BitbucketApiProxy
    {

        private string _apiUrl = "";

        public string ApiUrl
        {
            get { return _apiUrl; }
            set { _apiUrl = value; }

        }

        private string _accessToken = "";

        public string AccessToken
        {
            get { return _accessToken; }
            set { _accessToken = value; }

        }

        private readonly BaseApiProxy _apiCall = new BaseApiProxy();

        public IRestResponse<Response> GetCommitsForBranch(string branchName)
        {
            var parameters = new List<Parameter>
            {
                new Parameter{ Name = "branch", Value = branchName, Type = ParameterType.UrlSegment}
            };

            //return _apiCall.GetItem<Response>(_apiUrl, "2.0/repositories/{TDSiDev}/{tallisker}/commits", parameters, _accessToken);
            return _apiCall.GetItem<Response>(_apiUrl, "2.0/repositories/TDSiDev/tallisker/commits", parameters, _accessToken);
        }

        public IRestResponse<AccessToken> GetAccessToken(string clientKey, string clientSecret)
        {
            var client = new RestClient("https://bitbucket.org");
            var request = new RestRequest("site/oauth2/access_token", Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "client_credentials");
            request.AddParameter("client_id", clientKey);
            request.AddParameter("client_secret", clientSecret);
            //request.AddParameter("scope", "repository");
            return client.Execute<AccessToken>(request);
        }
    }
}