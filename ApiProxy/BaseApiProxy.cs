﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using Login_MVC_.Utilities;
using Microsoft.Owin.Security;
using NLog;
using RestSharp;
using static Login_MVC_.Utilities.RestSharpJsonDeserialiser;

namespace Login_MVC_.ApiProxy
{
    public class BaseApiProxy
    {

        [Obsolete]
        public IRestResponse<T> GetItem<T>(string apiUrl, string apiAction, List<Parameter> parameters, string accessToken) where T : class, new()
        {
            var client = new RestClient(apiUrl);
            //client.AddHandler("application/json", Default);
            client.AddHandler("site/oauth2/access_token", Default);
            var request = new RestRequest($"{apiAction}", Method.GET);
            request.AddHeader("Authorization", $"Bearer {accessToken}");

            foreach (var parameter in parameters)
            {
                if (parameter.Type == ParameterType.UrlSegment)
                    request.Resource += "/{" + parameter.Name + "}";

                request.AddParameter(parameter);
            }

            var response = client.Execute<T>(request);

            return response;
        }

        private string GetAccessToken()
        {
            var accessToken =
                ClaimsPrincipal.Current.Claims.FirstOrDefault(
                    c => c.Type == ClaimsUrl.GetClaimUrl(ClaimsConstants.ClaimAccessToken));

            if (accessToken != null)
                return accessToken.Value;
            throw new UnauthorizedAccessException("No access token found");
        }
    }
}