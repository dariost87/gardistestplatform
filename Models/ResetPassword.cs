﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Login_MVC_.Models
{
    public class ResetPassword
    {
        public Guid Token { get; set; }

        [Key] public int UserId { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}$")] 
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}