﻿using System;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.Security;
using Login_MVC_.DLayer;
using Login_MVC_.Models;
using WebMatrix.WebData;

namespace Login_MVC_.Controllers
{
    public class ForgotPasswordController : Controller
    {
        // GET: ResetPassword
        public ActionResult ResetPassword()
        {
            return View();
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string UserName)
        {
            var dal = new UserDal();

            var user = Membership.GetUser(UserName);

            if (user == null)

            {
                TempData["Message"] = "User Not exist.";
            }
            else
            {
                var token = WebSecurity.GeneratePasswordResetToken(UserName);

                var resetLink = "<a href='" +
                                Url.Action("ResetPassword", "ForgotPassword", new {un = UserName, rt = token}, "http") +
                                "'>Reset Password</a>";

                var Email = (from i in dal.UserAccounts where i.UserName == UserName select i.Email).FirstOrDefault();

                var subject = "Password Reset Token";
                var body = $"<font color='black'>Dear  </font><font color='green'>{user.UserName}<font><BR/>" +
                           $"<font color='black'><BR/>Please click link below to reset your password,<BR/>" +
                           $"<BR/> {resetLink}<BR/>" + //edit it
                           $"<BR/><font color='black'>Regards<BR/>" + $"Development Team</font>";

                try
                {
                    SendEmail(Email, subject, body);
                    ViewBag.pass =
                        "Mail has been sent successfully. You now can close this website and follow instruction sent to your email.";
                }
                catch (Exception ex)
                {
                    ViewBag.fail = "Error occured while sending Email." + ex.Message;
                }

                //ViewBag.fail = resetLink;
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPassword resetPassword, string rt)
        {
            var dal = new UserDal();
            //TODO: Check the un and rt matching and then perform following
            //get userid of received username
            var userid = (from i in dal.UserAccounts where i.UserName == resetPassword.UserName select i.UserId)
                .FirstOrDefault();
            //check userid and token matches
            var any = (from j in dal.webpages_Memberships
                where j.UserId == userid && j.PasswordVerificationToken == rt
                //&& (j.PasswordVerificationTokenExpirationDate < DateTime.Now)
                select j).Any();

            if (!ModelState.IsValid)
            {
                ViewBag.fail = "Something went wrong! Please try again.";
                return View();
            }

            if (!WebSecurity.ResetPassword(rt, resetPassword.Password))
            {
                ViewBag.fail = "Something went wrong! Please try again.";
                return View();
            }

            WebSecurity.Login(resetPassword.UserName, resetPassword.Password);

            //return RedirectToAction("Login", "Account");
            return RedirectToAction("Login", "Account");
        }

        private void SendEmail(string Email, string subject, string body)
        {
            var client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //client.EnableSsl = true;
            client.Host = "tdsi-co-uk.mail.protection.outlook.com";
            client.Port = 25;
            var msg = new MailMessage();
            msg.From = new MailAddress("gardisplatform@tdsi.co.uk");
            msg.To.Add(new MailAddress(Email));

            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;

            client.Send(msg);
        }
    }
}