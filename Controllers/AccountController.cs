﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Principal;
using System.ServiceModel.Security;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Login_MVC_.ApiProxy;
using Login_MVC_.DLayer;
using Login_MVC_.Models;
using Login_MVC_.Utilities;
using Microsoft.Owin.Security;
using Microsoft.Web.Mvc;
using WebMatrix.WebData;
using Login = Login_MVC_.Models.Login;

namespace Login_MVC_.Controllers
{
    //[Authorize]
    //[Authorize]
    public class AccountController : Controller
    {
        [Authorize]
        //[AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {

            return View();

        }

        //Login

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (TempData.ContainsKey("userName"))
            {
                ViewBag.pass = TempData["userName"] + " successfully registered.";
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult LoginNewUser(string userName)
        {
            TempData["userName"] = userName;

            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(Login account)
        {
            UserDal dal = new UserDal();
            //Encryption enc = new Encryption();
            var login = false;
            if (!ModelState.IsValid)
            {
                ViewBag.fail = "Invalid Username/Password!";
                return View("Login");
            }


            if (!WebSecurity.Login(account.UserName, account.Password))
            {
                ViewBag.fail = "Invalid Username/Password!";
                return View("Login");
                

            }
            if (WebSecurity.Login(account.UserName, account.Password))
            {
                ViewBag.pass = "Login Successful!";
                //account = new Login();
                
                FormsAuthentication.SetAuthCookie("Hello, " + account.UserName, true);
                Response.Redirect("Account/Index");

            }

            return RedirectToAction("Index");

        }

        public ActionResult Logout()
        {

            FormsAuthentication.SignOut();
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            return RedirectToAction("Login", "Account");
        }
    }
}


