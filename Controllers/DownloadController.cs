﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace Login_MVC_.Controllers
{
    [Authorize]
    public class DownloadController : Controller
    {
        // GET: Download
        // [AllowAnonymous]
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult DownloadGardisRelease()
        {
            //return File(@"C:\Temp\psagent.log", System.Net.Mime.MediaTypeNames.Application.Zip, "psagent.zip");
            return File(@"C:\Downloads\GardisRelease\Gardis.zip", System.Net.Mime.MediaTypeNames.Application.Zip, "Gardis.zip");
        }

        public ActionResult DownloadRelease2Development()
        {
            return File(@"", System.Net.Mime.MediaTypeNames.Application.Zip, "");
        }
    }
}