﻿using Login_MVC_.DLayer;
using Login_MVC_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace Login_MVC_.Controllers
{
    public class UserSettingsController : Controller
    {
        // GET: UserSettings
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdatePassword(Login account)
        {
            var login = false;
            if (!ModelState.IsValid)
            {
                ViewBag.fail = "Invalid Username/Password!";
                return View("Index");
            }


            if (!WebSecurity.Login(account.UserName, account.Password))
            {
                ViewBag.fail = "Invalid Username/Password!";
                return View("Index");
            }

            return View();
        }
    }
}
