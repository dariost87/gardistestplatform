﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Login_MVC_.DLayer;
using Login_MVC_.Models;
using WebMatrix.WebData;


namespace Login_MVC_.Controllers
{
    public class RegistrationController : Controller
    {

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Registration user)
        {
            var dal = new UserDal();
            //var enc = new Encryption();

            if (ModelState.IsValid)
            {
                if (dal.UserAccounts.Any(x => x.UserName == user.UserName))
                {
                    ViewBag.userexists = "User Already Exists!";
                }
                else
                {
                    

                    var userAccount = new User
                    {
                        UserId = user.UserId,
                        UserName = user.UserName,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Password = user.Password
                    };
                   // var hashedPassword = enc.CreateHash(user.Password);

                    WebSecurity.CreateUserAndAccount(userAccount.UserName, user.Password, new { Email = user.Email, FirstName = user.FirstName, LastName = user.LastName});
                    WebSecurity.Login(userAccount.UserName, user.Password);

                   // userAccount.Password = hashedPassword;

                    var m = new MailMessage(
                        new MailAddress("gardistestplatform@tdsi.co.uk",
                            "Gardis Test Platform Registration"),
                        new MailAddress(user.Email));
                    m.Subject = "Registration confirmation";
                    m.Body = $"Dear {user.FirstName + " " + user.LastName}<BR/>" +
                             "<BR/>Thank you for registration,<BR/>" +
                             $"<BR/>You can now login using User Name:  <font color='green'>{user.UserName}<font><BR/>" +
                             "<BR/><a href='http://192.168.5.28:5069'>Open Login Page</a><BR/>" +
                             "<BR/><font color='black'>Regards<BR/>" +
                             "Development Team</font>";

                    m.IsBodyHtml = true;
                    var smtp =
                        new SmtpClient("tdsi-co-uk.mail.protection.outlook.com");
                    //    new SmtpClient("smtp.gmail.com");
                    //smtp.Port = 587;
                    //smtp.UseDefaultCredentials = false;

                    //smtp.EnableSsl = true;
                    //smtp.Credentials = new NetworkCredential("dariusz.stachowski335@gmail.com", "Bimmer325");
                    smtp.Send(m);

                    ModelState.Clear();

                    return RedirectToAction("LoginNewUser", "Account", new {userName = user.FirstName + " " + user.LastName});
                }
            }
            else
            {
                ViewBag.fail = "Confirm password doesn't match, Type again !";
            }
            
            return View();

        }
    }
}