﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Login_MVC_.Controllers
{
  
    public class SiteController : Controller
    {
        // GET: Site
        [Authorize]
        //[AllowAnonymous]
        public ActionResult ChangeLog()
        {
            return View();
        }
    }
}