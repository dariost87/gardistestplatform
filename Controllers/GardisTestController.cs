﻿using Login_MVC_.ApiProxy;
using Login_MVC_.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

    namespace Login_MVC_.Controllers
    {
        public class GardisTestController : Controller
        {


            // GET: TestResults
            public ActionResult Index()
            {

                var apiProxy = new BitbucketApiProxy();

                var lastCommitMaster = GetLastCommit(apiProxy, "master");

                var lastCommitRelease = GetLastCommit(apiProxy, "release2development");

                var lastCommitTcReleaseMain = GetLastCommit(apiProxy, "TeamCity_ReleaseMain");

                return View(new List<Commit> { lastCommitMaster, lastCommitRelease, lastCommitTcReleaseMain });

                //return PartialView("Index", new List<Commit> { lastCommitMaster, lastCommitRelease, lastCommitTcReleaseMain });
                //return View();

            }
            public ActionResult MasterBranchCommits(BitbucketApiProxy apiProxy)
            {
                var lastCommit = GetLastCommit(apiProxy, "master");

                return Json(lastCommit);
            }

            private Commit GetLastCommit(BitbucketApiProxy apiProxy, string branchName)
            {
                apiProxy.ApiUrl = "https://api.bitbucket.org";

                var accessToken = apiProxy.GetAccessToken("Yv6wtmpT3yP4uwn5Us", "htqHCUqYaDAzbaAbQ4MFkgLSdMxh9Dpq");

                apiProxy.AccessToken = accessToken.Data.access_token;

                var response = apiProxy.GetCommitsForBranch(branchName);

                var commit = response.Data.Values.FirstOrDefault();

                commit.name = branchName;

                return commit;
            }
        }
    }
