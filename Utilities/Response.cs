﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Login_MVC_.Utilities
{
    public class Response
    {
        //public int size { get; set; }
        //public int limit { get; set; }
        //public bool isLastPage { get; set; }


        public List<Commit> Values { get; set; }
        public int Start { get; set; }


        //public object filter { get; set; }
        //public int nextPageStart { get; set; }

    }

    public class Self
    {
        //public string href { get; set; }

        public string Name { get; set; }
    }

    public class Html
    {
        //public string href { get; set; }
        public string Name { get; set; }
    }

    public class Avatar
    {
        //public string href { get; set; }
        public string Name { get; set; }
    }

    public class Followers
    {
        //public string href { get; set; }
        public string Name { get; set; }
    }

    public class Following
    {
        //public string href { get; set; }
        public string Name { get; set; }
    }

    public class Repositories
    {
        //public string href { get; set; }
        public string Name { get; set; }
    }

    public class Links
    {
        //public Self self { get; set; }
        //public Html html { get; set; }
        //public Avatar avatar { get; set; }
        //public Followers followers { get; set; }
        //public Following following { get; set; }
        //public Repositories repositories { get; set; }
    }

    public class User
    {
        //public Links links { get; set; }
        //public string username { get; set; }
        //public string nickname { get; set; }
        //public string account_status { get; set; }
        //public string display_name { get; set; }
        //public string website { get; set; }
        //public string created_on { get; set; }
        //public string uuid { get; set; }
        //public string has_2fa_enabled { get; set; }
        public string Type { get; set; }
    }

    public class Author
    {
        public string raw { get; set; }
        public User User { get; set; }
        public string Type { get; set; }
    }

    public class Summary
    {
        public string raw { get; set; }
        public string markup { get; set; }
        public string html { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
        public string name { get; set; }
    }

    public class Html2
    {
        public string href { get; set; }
        public string name { get; set; }
    }

    public class Avatar2
    {
        public string href { get; set; }
        public string name { get; set; }
    }

    public class Followers2
    {
        public string href { get; set; }
        public string name { get; set; }
    }

    public class Following2
    {
        public string href { get; set; }
        public string name { get; set; }
    }

    public class Repositories2
    {
        public string href { get; set; }
        public string name { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
        public Html2 html { get; set; }
        public Avatar2 avatar { get; set; }
        public Followers2 followers { get; set; }
        public Following2 following { get; set; }
        public Repositories2 repositories { get; set; }
    }

    public class User2
    {
        public Links2 links { get; set; }
        public string username { get; set; }
        public string nickname { get; set; }
        public string account_status { get; set; }
        public string display_name { get; set; }
        public string type { get; set; }
    }

    public class Author2
    {
        public string raw { get; set; }
        public User2 user { get; set; }
        public string type { get; set; }
    }

    public class Parent
    {
        public string hash { get; set; }
        public string date { get; set; }
        public Author2 author { get; set; }
        public string type { get; set; }
    }

    public class Commit
    {
        public string name { get; set; }
        public string hash { get; set; }
        public string date { get; set; }
        public Author author { get; set; }
        public string message { get; set; }
        public Summary summary { get; set; }
        public List<Parent> parents { get; set; }
        public string type { get; set; }
    }

}
