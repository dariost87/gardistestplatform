﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Login_MVC_.Utilities
{
    public static class ClaimsConstants
    {
        //public const string NoUser = "No user name found";
        //public const string NoUserRole = "No role for the user has been specified";
        //public const string ClaimRoleName = "Role";


        public const string ClaimAccessToken = "access_token";


        //public const string ClaimRefreshToken = "refresh_token";
        //public const string ClaimIdToken = "id_token";
        //public const string ClaimAccessTokenExpiresAt = "expires_at";
        //public const string UserId = "UserId";


        public const string ClaimBaseUrl = "//TDSi/";


        //public const string ActionCreate = "Create";
        //public const string ActionWrite = "Write";
        //public const string ActionRead = "Read";
        //public const string ActionDelete = "Delete";
        //public const string ActionCanUse = "CanUse";
        //public const string ActionExtendedUse = "ExtendedUse";
        //public const string RoleManagement = "RoleManagement";
        //public const string ScheduleManagement = "ScheduleManagement";
        //public const string HolidayManagement = "HolidayManagement";
        //public const string UserManagement = "UserManagement";  //User sare under role management
        //public const string OrganisationManagement = "OrganisationManagement";
        //public const string DepartmentManagement = "OrganisationManagement";
        //public const string PeopleManagement = "PeopleManagement";    //People rights are under organisations. (Child Actions)
        //public const string SiteManagement = "SiteManagement";
        //public const string AreaManagement = "SiteManagement";
        //public const string CredentialManagement = "PeopleManagement";    // Same as people
        //public const string EquipmentManagement = "EquipmentManagement";
        //public const string ComputerManagement = "EquipmentManagement";
        //public const string AcuServiceManagement = "EquipmentManagement";
        //public const string ChannelManagement = "EquipmentManagement";
        //public const string DeviceManagement = "EquipmentManagement";            //Acu Management is user SiteManagement. (Child Actions)
        //public const string AccessLevelManagement = "AccessLevelManagement";
        //public const string EventManagement = "EventManagement";
        //public const string UserEventManagement = "UserEventManagement";
        //public const string EventLogManagement = "EventManagement";
        //public const string AlarmManagement = "EventManagement";
        //public const string AcuManager = "AcuManager";
        //public const string PurgeManagement = "PurgeManagement";
        //public const string SystemManagement = "SystemManagement";


        //public const string EquipmentStatusManagement = "EquipmentStatusManagement";


        //public const string ReportManagement = "ReportManagement";
        //public const string FireDoorManagement = "FireDoorManagement";
        //public const string LockdownDoorManagement = "LockdownDoorManagement";
        //public const string PasswordRestToken = "PasswordRest";
        //public const string Language = "Language";

        public static string GetClaimUrl(string subName)
        {
            return $"{ClaimsConstants.ClaimBaseUrl}{subName}";
        }
    }
}