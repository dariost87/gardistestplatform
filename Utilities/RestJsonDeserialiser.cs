﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using RestSharp.Deserializers;
using RestSharp;
using Newtonsoft.Json;
using NLog;
using NLog.Fluent;

namespace Login_MVC_.Utilities
{
    public class RestSharpJsonDeserialiser : IDeserializer
    {
        private static readonly Lazy<RestSharpJsonDeserialiser> LazyInstance =
            new Lazy<RestSharpJsonDeserialiser>(() => new RestSharpJsonDeserialiser());

        private readonly JsonSerializerSettings _settings;

        private static readonly Logger Log = LogManager.GetLogger("API response deserialiser");

        public static RestSharpJsonDeserialiser Default => LazyInstance.Value;

        public RestSharpJsonDeserialiser()
        {
            _settings = new JsonSerializerSettings
            {
                 //Error = ErrorHandler
            };
        }

        public T Deserialize<T>(IRestResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                case HttpStatusCode.Forbidden:

                    Log.Error($"Access denied requesting API URL {response.Request.Resource}: {response.Content}");

                    throw new WebException(
                        $"Access denied requesting API URL {response.Request.Resource}: {response.Content}");
                case HttpStatusCode.BadRequest:
                    Log.Error($"Bad request error requesting API URL {response.Request.Resource}: {response.Content}");

                    throw new WebException(
                        $"Bad request error requesting API URL {response.Request.Resource}: {response.Content}");
                case HttpStatusCode.InternalServerError:
                    Log.Error(

                         $"Internal server error requesting API URL {response.Request.Resource}: {response.Content}");

                    throw new WebException(
                        $"Internal server error requesting API URL {response.Request.Resource}: {response.Content}");
            }

            return JsonConvert.DeserializeObject<T>(response.Content, _settings);
        }
    }
}