﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Login_MVC_.Utilities
{
    public static class ClaimsUrl
    {
        public static string GetClaimUrl(string subName)
        {
            return $"{ClaimsConstants.ClaimBaseUrl}{subName}";
        }
    }
}

